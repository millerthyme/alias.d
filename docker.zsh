#!/bin/zsh

# General Docker Aliases
alias dipa="docker image prune -a -f"
alias dps="docker ps -a"
alias drst="docker restart"
alias dnc="docker network create"
alias dni="docker network inspect"

# Docker Compose Aliases
# This uses Docker Compose V2 (for V1 use 'docker-compose')
alias dcb="docker compose build"
alias dcupd="docker compose up -d"
alias dcup="docker compose up"
alias dcdn="docker compose down"

