#!/bin/zsh

alias sudo="doas"
alias update="sudo apt update && sudo apt upgrade -y"
alias la="ls -al"
alias ll="ls -l"

